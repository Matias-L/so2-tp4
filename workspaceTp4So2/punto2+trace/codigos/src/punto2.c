/*Alumno: Ceballos, Matias Lionel
 * SO2---TP4
 * punto 2
 * Consigna:Crear un programa con dos tareas simples (productor/consumidor)
 * y realizar un análisis completo del Sistema con Tracealyzer (tiempos de ejecución, memoria )
 *
 *Breve explicacion:
 *El recurso compartido sera una pila de estructuras del tipo "elemento". La pila tiene un tamaño
 *"TAM_PILA". Dentro de la estructura "elemento" hay una variable caracter, que será seteada
 *por el productor, y leida por el consumidor. Al mismo tiempo, la variable "indice" llevara la
 *cuenta de cuantos elementos hay en la pila. El productor la incrementa y el consumidor la decrementa.
 *
 *Para asegurar la exclucion mutua, se hace uso de un semaforo.
 *Cada task debera pedir el semaforo antes de intentar modificar el recurso. Si lo obtiene, lo
 *modifica y devuelve el semaforo. Si no, espera y luego prueba nuevamente.
 *Se les ha dado a ambas tareas el mismo nivel de prioridad.
 *
 *
 *Hago uso de la funcion vTraceStop() para detener el registro de datos de +trace.
 *Para eso, agrego una variable contador que se incrementara en 1 cada vez que se ejecute el
 *consumidor, y en el mismo se impone el valor limite del contador hasta detener al trace.
 * */

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"

#define LIMITE_TRACE 250	//Limitador para +trace. Para no llenar demasiado la memoria
#define DEMORA_PROD 1250	//Establezco demoras, expresadas en "ticks"
#define DEMORA_CONS 1500
#define TAM_PILA	5


xSemaphoreHandle xSemaphore = NULL;//Semaforo como variable global
size_t indice=0;				//Lleva la cuenta de cuantos elementos hay en la pila
int contador=0;

struct elemeto{
	char letra;
};

struct elemeto pila[TAM_PILA]={NULL, NULL, NULL, NULL, NULL};

//Inicializa hardware
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();			//Inicializacion general de perifericos, como UART
	Board_LED_Set(0, false); //Inicio con LED apagado
	DEBUGOUT("Hard OK.");
}

//Tarea productor
static void vProductorTask(void *pvParameters) {
	while (1) {
		if( xSemaphore != NULL )
		    {
		        /* Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY */
		        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
		        {   /* Obtenido el semaforo, manejamos el recurso compartido*/
		            indice=indice+1;
		            if(indice<TAM_PILA){
		            	struct elemeto elem;
		            	elem.letra = 'A';
			            pila[indice]=elem;
			            Board_LED_Set(0, true);		//Prende LED
			            DEBUGOUT("Productor: %d || %c\r\n", indice, pila[indice].letra);
		            }
		            else{
			            DEBUGOUT("P.DES.: %d\r\n", indice);
		            	indice = TAM_PILA-1;
		            }
		            /* Finalizado el uso del recurso compartido, devolvemos el semaforo */
		            xSemaphoreGive( xSemaphore );
		        }
		        else
		        {
		            /* Se ingresa aca si no se pudo obtener el semaforo. Hago nada*/
		        }
		    }
		vTaskDelay(DEMORA_PROD);	//Demora de la tarea
	}//Fin while(1)
}

//Tarea consumidor
static void vConsumidorTask(void *pvParameters) {
	while (1) {
				if( xSemaphore != NULL )
				    {/* Verifica si el semaforo esta libre. si no, espera portMAX_DELAY. */
				        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
				        {
				            /* Otenido el semaforo, manipulamos el recurso compartido*/
							contador=contador+1;
							if(contador==LIMITE_TRACE){
								vTraceStop();
							}
				            if(indice>0){
				            char letra_obtenida = pila[indice].letra;
				            pila[indice].letra = ' ';//pila[indice]=NULL;
				            indice=indice-1;
				            Board_LED_Set(0, false);
				            DEBUGOUT("Consumidor: %d || %c\r\n", indice, letra_obtenida);
				            }
				            /* Liberado el recurso, devolvemos el semaforo*/
				            xSemaphoreGive( xSemaphore );
				        }
				        else
				        {
				            // Se ingresa aqui si no se pudo obtener el semaforo. Hago nada
				        }
				    }
		vTaskDelay(DEMORA_CONS);
	}//Fin while(1)
}

/**
 * @brief	main routine for FreeRTOS blinky example
 * @return	Nothing, function should not exit
 */
int main(void)
{
	vTraceInitTraceData();
	uiTraceStart();
	prvSetupHardware();
	xSemaphore = xSemaphoreCreateMutex(); //Inicializa mutex

	/* Crea tarea productor */
	xTaskCreate(vProductorTask, (signed char *) "vProductorTask",
				configMINIMAL_STACK_SIZE, NULL,1,
				(xTaskHandle *) NULL);

	/* Crea tarea consumidor*/
	xTaskCreate(vConsumidorTask, (signed char *) "vConsumidorTask",
				configMINIMAL_STACK_SIZE, NULL, 1,
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
