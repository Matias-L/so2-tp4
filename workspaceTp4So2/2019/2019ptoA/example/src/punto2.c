/*Alumno: Ceballos, Matias Lionel
 * SO2---TP4
 * punto 2
 * Consigna:Crear un programa con dos tareas simples (productor/consumidor)
 * y realizar un análisis completo del Sistema con Tracealyzer (tiempos de ejecución, memoria )
 *
 *
 *
 *Breve explicacion:
 *El recurso compartido sera una variable "acumulador", que se incrementará en 1
 *cada vez que entre el productor, y decrementara en 1 cada vez que se ejecute el
 *consumidor. Para asegurar la exclucion mutua, se hace uso de un semaforo.
 *Cada task debera pedir el semaforo antes de intentar modificar el recurso. Si lo obtiene, lo
 *modifica y devuelve el semaforo. Si no, espera y luego prueba nuevamente.
 *Se les ha dado a ambas tareas el mismo nivel de prioridad.
 *
 *
 *Hago uso de la funcion vTraceStop() para detener el registro de datos de +trace.
 *Para eso, agrego una variable contador que se incrementara en 1 cada vez que se ejecute el
 *consumidor, y en el mismo se impone el valor limite del contador hasta detener al trace.
 * */

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"

#define LIMITE_TRACE 250

xSemaphoreHandle xSemaphore = NULL;//Semaforo como variable global
size_t acumulador=0;
int contador=0;


/* Sets up system hardware */
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);
}

//Tarea productor
static void vProductorTask(void *pvParameters) {
	while (1) {

		if( xSemaphore != NULL )
		    {
		        /* Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY */
		        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
		        {
		            /* Obtenido el semaforo, manejamos el recurso compartido*/

		            acumulador=acumulador+1;
		            Board_LED_Set(0, true);
		            DEBUGOUT("Productor: %d\r\n", acumulador);

		            /* Finalizado el uso del recurso compartido, devolvemos el semaforo */
		            xSemaphoreGive( xSemaphore );
		        }
		        else
		        {
		            /* Se ingresa aca si no se pudo obtener el semaforo*/
		        }
		    }

		/* About a 3Hz on/off toggle rate */
		vTaskDelay(configTICK_RATE_HZ / 6);
	}
}

//Tarea consumidor
static void vConsumidorTask(void *pvParameters) {
	while (1) {

				if( xSemaphore != NULL )
				    {
				        /* Verifica si el semaforo esta libre. si no, espera portMAX_DELAY. */
				        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
				        {
				            /* Otenido el semaforo, manipulamos el recurso compartido*/

							contador=contador+1;
							if(contador==LIMITE_TRACE){
								vTraceStop();
							}

				            if(acumulador>0){
							acumulador=acumulador-1;
				            Board_LED_Set(1, false);
				            DEBUGOUT("Consumidor: %d\r\n", acumulador);
				            }

				            /* Liberado el recurso, devolvemos el semaforo*/
				            xSemaphoreGive( xSemaphore );
				        }
				        else
				        {
				            /* Se ingresa aqui si no se pudo obtener el semaforo */
				        }
				    }

		/* About a 7Hz on/off toggle rate */
		vTaskDelay(configTICK_RATE_HZ / 14);
	}
}

/**
 * @brief	main routine for FreeRTOS blinky example
 * @return	Nothing, function should not exit
 */
int main(void)
{
	vTraceInitTraceData();
	uiTraceStart();
	prvSetupHardware();
	xSemaphore = xSemaphoreCreateMutex(); //Inicializa mutex

	/* Crea tarea productor */
	xTaskCreate(vProductorTask, (signed char *) "vProductorTask",
				configMINIMAL_STACK_SIZE, NULL,1,
				(xTaskHandle *) NULL);

	/* Crea tarea consumidor*/
	xTaskCreate(vConsumidorTask, (signed char *) "vConsumidorTask",
				configMINIMAL_STACK_SIZE, NULL, 1,
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
