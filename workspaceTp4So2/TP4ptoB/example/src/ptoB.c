
/*
 * Alumno: Ceballos, Matias Lionel
 *
 * Consigna punto 3:
 * Diseñe   e   implemente   una   aplicación   que   posea   dos   productores
 * y   un   consumidor.   El primero   de   los   productores   es   una   tarea
 * que   genera   strings   de   caracteres   de   longitud variable
 * (ingreso   de   comandos   por   teclado).   La   segunda   tarea,   es   un
 * valor   numérico   de longitud   fija,   proveniente   del   sensor   de
 * temperatura   del   embebido.   También   que   la primer tarea es aperiódica
 * y la segunda periódica definida por el diseñador.
 * Por   otro   lado,   el   consumidor,   es   una   tarea   que   envía
 * el valor recibido a la terminal de una computadora por puerto serie (UART)
 *
 * */


#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"
#include "chip.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>




#define IRQ_SELECTION 	UART3_IRQn
#define _ADC_CHANNLE ADC_CH0
#define _LPC_ADC_ID LPC_ADC
#define _LPC_ADC_IRQ ADC_IRQn



xSemaphoreHandle xSemaphore = NULL;//Semaforo como variable global
//size_t acumulador=0;
char *pila[5];		//Pila donde se agregara lo del productor, y de donde quitara el consumidor
						//Espacio para 5 renglones, y cadenas de 25 caracteres maximo
int index=0;		//Indice de la pila
int listoRecepcion=0;
char* cadenaUart;
char* cadenaUartAux;	//Cadena antes de concatenar
char caracter;
int caracterEntrada;


static ADC_CLOCK_SETUP_T ADCSetup;
static volatile uint8_t Burst_Mode_Flag = 0, Interrupt_Continue_Flag;
static volatile uint8_t ADC_Interrupt_Done_Flag, channelTC, dmaChannelNum;

/* Sets up system hardware */
static void prvSetupHardware(void)
{

	SystemCoreClockUpdate();
	Board_UART_Init(LPC_UART3);
	Board_Init();
	DEBUGOUT("xXx \n");

	//Configura ADC
	/*ADC Init */
	uint32_t _bitRate = ADC_MAX_SAMPLE_RATE;
	Chip_ADC_Init(_LPC_ADC_ID, &ADCSetup);
	Chip_ADC_EnableChannel(_LPC_ADC_ID, _ADC_CHANNLE, ENABLE);

}

//Tarea productor 1: UART
/*
 * El
*primero   de   los   productores   es   una   tarea
*que   genera   strings   de   caracteres   de   longitud
*variable   (ingreso   de   comandos   por   teclado).
 * */
static void vProductor1Task(void *pvParameters) {
	while (1) {
		caracterEntrada=Board_UARTGetChar();		        //Recibe la decena
		if(caracterEntrada!=EOF){
			if(caracterEntrada!=27){	//Si no se recibe un escape
				//DEBUGOUT("Tengo caracter \n");
				caracter=(char)caracterEntrada;
				//Otra forma, usando https://stackoverflow.com/questions/10279718/append-char-to-string-in-c

				size_t len=strlen(cadenaUart);
				char* str2= malloc(len+1+1);
				strcpy(str2, cadenaUart);
				str2[len]=caracter;
				str2[len + 1] = '\0';
				//Ya tengo en str2 la cadena "nueva". La paso a cadenaUart y limpio mem str2
				free(cadenaUart);
				len=strlen(str2);
				cadenaUart=malloc(len+1+1);
				strcpy(cadenaUart, str2);
				free(str2);


				//str2[len+1]='\0';
				/*cadenaUart=(char*)malloc(strlen(cadenaUartAux)+1+4);
				strcpy(cadenaUart, cadenaUartAux);
				strcat(cadenaUart, caracter);

				free(cadenaUartAux);
				cadenaUartAux=malloc(cadenaUart);
				strcpy(cadenaUartAux, cadenaUart);

*/
				/*
				 * CadenaUart=cadenaUartAux+nuevo caracter
				 * CadenaUart siempre es algo mas grande que cadenaUartAux.
				 * Esta ultima es usada para mantener el estado anterior de la cadena
				 * Tambien se usa como referencia para alocar memoria de cadenaUart
				 * Por eso, primero aloco memoria del tamaño anterior (cadenaUartAux) mas
				 * 4 bytes para el nuevo caracter, mas uno para el caracter de finalizacion
				 * (creo que este no hace falta. Voy a alternar entre usarlo y no para probar)
				 * Luego, copio la cadena auxiliar en la cadena UART
				 * Finalmente, limpio la cadena auxiliar, y en ella copio el estado mas
				 * nuevo de cadenaUart
				 *
				 * */

			}
			else{
				//DEBUGOUT("Escape \n\n");
				//Se ingreso escape, por lo tanto, se actua
				//Primero hago entrada por teclado, y finalizado, pido semaforo
				//Asi espero bloquear el recurso por el tiempo de reaccion humano
				if( xSemaphore != NULL )
				    {
				        /* Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY */
				        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
				        {
				            /* Obtenido el semaforo, manejamos el recurso compartido*/

				        	if(index>=5){
				        		//DEBUGOUT("Desborde de pila task1UART.
				        		//Se pierde ultimo valor \n");(no sobreescribe)
				        	}
				        	else{
				        		//DEBUGOUT("Copiando a la pila: %s \n", cadenaUart);

								pila[index]=(char*)malloc(strlen(cadenaUart));
								strcpy(pila[index], cadenaUart);



				        		//strcpy(pila[index], cadenaUart);
				        		index=index+1;
				        	}

				            //DEBUGOUT("ProductorUART: %s\r\n", pila[index]);
				            free(cadenaUart);	//Como ya usamos esta cadena, la limpiamos
				            /* Finalizado el uso del recurso compartido, devolvemos el semaforo */
				            xSemaphoreGive( xSemaphore );
				        }
				        else
				        {
				            /* Se ingresa aca si no se pudo obtener el semaforo*/
				        }
				    }//Fin xSemaphore!=Null
			}//Fin else escape

		}//Fin caracter!=EOF
	// About a 3Hz on/off toggle rate (no mover de aca)
		//Como es aperiodico no hace falta agregar el delay
	//vTaskDelay(configTICK_RATE_HZ / 6);
	}//Fin while(1)
}


//Tarea productor 2: ADC
static void vProductor2Task(void *pvParameters) {
	while (1) {


		uint16_t dataADC;
		char valorAdc[6];

		/* Select using burst mode or not ¿hace falta? */
		if (Burst_Mode_Flag) {
			Chip_ADC_SetBurstCmd(_LPC_ADC_ID, ENABLE);
		}
		else {
			Chip_ADC_SetBurstCmd(_LPC_ADC_ID, DISABLE);
		}

			/* Start A/D conversion if not using burst mode */
			if (!Burst_Mode_Flag) {
				Chip_ADC_SetStartMode(_LPC_ADC_ID, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
			}
			/* Waiting for A/D conversion complete */
			while (Chip_ADC_ReadStatus(_LPC_ADC_ID, _ADC_CHANNLE, ADC_DR_DONE_STAT) != SET) {}
			/* Read ADC value */
			Chip_ADC_ReadValue(_LPC_ADC_ID, _ADC_CHANNLE, &dataADC);
			/* Print ADC value */


		/* Disable burst mode, if any */
		if (Burst_Mode_Flag) {
			Chip_ADC_SetBurstCmd(_LPC_ADC_ID, DISABLE);
		}

//Ya tengo el valor del ADC, lo paso a string para almacenarlo en la lista

sprintf(valorAdc, "%u", dataADC);//Revisar. Aca se rompe

//Teniendo el resiltado, pido el semaforo
		if( xSemaphore != NULL )
						    {
						        /* Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY */
						        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
						        {
						            /* Obtenido el semaforo, manejamos el recurso compartido*/

						        	if(index>=5){
						        		//DEBUGOUT("Desborde de pila task1UART.
						        		//Se pierde ultimo valor \n");(no sobreescribe)
						        	}
						        	else{
						        		//DEBUGOUT("Copiando a la pila: %s \n", cadenaUart);

										pila[index]=(char*)malloc(strlen(valorAdc));
										strcpy(pila[index], valorAdc);
						        		//strcpy(pila[index], cadenaUart);
						        		index=index+1;
						        	}

						            //DEBUGOUT("ProductorUART: %s\r\n", pila[index]);
						            free(valorAdc);	//Como ya usamos esta cadena, la limpiamos
						            /* Finalizado el uso del recurso compartido, devolvemos el semaforo */
						            xSemaphoreGive( xSemaphore );
						        }
						        else
						        {
						            /* Se ingresa aca si no se pudo obtener el semaforo*/
						        }










//DEBUGOUT("ADC");
		/*if(listoRecepcion){

		//Primero hago entrada por teclado, y finalizado, pido semaforo
		//Asi espero bloquear el recurso por el tiempo de reaccion humano


		if( xSemaphore != NULL )
		    {
		        // Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY
		        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
		        {
		            // Obtenido el semaforo, manejamos el recurso compartido

		        	if(index>=5){
		        		DEBUGOUT("Desborde de pila task1UART. Se pierde ultimo valor \n");
		        	}
		        	else{
			        	pila[index]="ADC";
		        		index=index+1;
		        	}

		            DEBUGOUT("ProductorUART: %s\r\n", pila[index]);

		            // Finalizado el uso del recurso compartido, devolvemos el semaforo
		            xSemaphoreGive( xSemaphore );
		        }
		        else
		        {
		            // Se ingresa aca si no se pudo obtener el semaforo
		        }
		    }
		listoRecepcion=0;
		// About a 3Hz on/off toggle rate
		vTaskDelay(configTICK_RATE_HZ / 6);
	}//Fin listo recepcion
*/

		// About a 3Hz on/off toggle rate (no mover de aca)
		vTaskDelay(configTICK_RATE_HZ / 6);
		}

}//fin while(1)

}

//Tarea consumidor
/*el   consumidor,   es   una   tarea   que   envía   el   valor
 *recibido   a   la   terminal   de   una
 *computadora   por   puerto   serie   (UART)
 * */
static void vConsumidorTask(void *pvParameters) {
	while (1) {

				if( xSemaphore != NULL )
				    {
				        // Verifica si el semaforo esta libre. si no, espera portMAX_DELAY.
				        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
				        {
				            // Otenido el semaforo, manipulamos el recurso compartido


				          // DEBUGOUT("Consumidor: %s\r\n", pila[index]);
				            if(index>0){
				            	index=index-1;
				            }
				            if(index<0){
				            	DEBUGOUT("\n **EPA, NO DEBERIAS VER ESTO CONSUMIDOR** \n");
				            	DEBUGOUT("Indice: %d", index);
				            }


				            // Liberado el recurso, devolvemos el semaforo
				            xSemaphoreGive( xSemaphore );
				        }
				        else
				        {
				            // Se ingresa aqui si no se pudo obtener el semaforo
				        }
				    }

		// About a 7Hz on/off toggle rate
		vTaskDelay(configTICK_RATE_HZ / 14);
	}
}


void UART3_IRQHandler(void)
{
	//DEBUGOUT("IRQHandler");
/*
	caracter=DEBUGIN();		        //Recibe la decena

	if(caracter!=27){	//Si no se recibe un escape
		DEBUGOUT("ESCAPE");
	strcat(cadenaUart, caracter);
	}
	else{
		listoRecepcion=1;
	}
*/

}
int main(void)
{
	prvSetupHardware();
	xSemaphore = xSemaphoreCreateMutex(); //Inicializa mutex

	/* Crea tarea productor 1 */
	xTaskCreate(vProductor1Task, (signed char *) "vProductor1Task",
				configMINIMAL_STACK_SIZE, NULL,1,
				(xTaskHandle *) NULL);

	/* Crea tarea productor 2 */
	xTaskCreate(vProductor2Task, (signed char *) "vProductor2Task",
				configMINIMAL_STACK_SIZE, NULL,1,
				(xTaskHandle *) NULL);

	/* Crea tarea consumidor*/
	xTaskCreate(vConsumidorTask, (signed char *) "vConsumidorTask",
				configMINIMAL_STACK_SIZE, NULL, 1,
				(xTaskHandle *) NULL);


	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
