/*
 * Alumno: Ceballos, Matias Lionel
 *
 * Consigna punto 3:
 * Diseñe   e   implemente   una   aplicación   que   posea   dos   productores
 * y   un   consumidor.   El primero   de   los   productores   es   una   tarea
 * que   genera   strings   de   caracteres   de   longitud variable
 * (ingreso   de   comandos   por   teclado).   La   segunda   tarea,   es   un
 * valor   numérico   de longitud   fija,   proveniente   del   sensor   de
 * temperatura   del   embebido.   También   que   la primer tarea es aperiódica
 * y la segunda periódica definida por el diseñador.
 * Por   otro   lado,   el   consumidor,   es   una   tarea   que   envía
 * el valor recibido a la terminal de una computadora por puerto serie (UART)
 *
 *Licencia tracelyzer: 20/06/2019
 * */
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"
#include "chip.h"

#define IRQ_SELECTION 	UART3_IRQn
#define UART_SELECTION 	LPC_UART3
#define HANDLER_NAME 	UART3_IRQHandler
#define _ADC_CHANNLE ADC_CH0
#define _LPC_ADC_ID LPC_ADC
#define _LPC_ADC_IRQ ADC_IRQn
#define LIMITE_TRACE 10
#define DEMORA_PROD_ADC 1000
#define DEMORA_PROD_UART 1250
#define DEMORA_CONS 900
#define TAM_PILA	5

xSemaphoreHandle xSemaphore = NULL;	//Semaforo como variable global

int index=0;						//Indice de la pila
int contador=0;
int palabraElejir=0;
char* arregloPalabras[6]={"hola", "hello", "hallo", "konichiwa", "shalom", "priviet"};

struct elemeto{
	//boolean esPalabra;
	uint16_t letra;
	char palabra[9];
};

struct elemeto pila[TAM_PILA];

xTaskHandle taskProductor1_handle;

static ADC_CLOCK_SETUP_T ADCSetup;
static volatile uint8_t Burst_Mode_Flag = 0, Interrupt_Continue_Flag;
static volatile uint8_t ADC_Interrupt_Done_Flag, channelTC, dmaChannelNum;

//Inicializa hardware
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();
	Board_UART_Init(LPC_UART3);	//Inicializa UART
	Board_Init();
	Board_LED_Set(0, false); //Inicio con LED apagado
	//Configura ADC
	Chip_ADC_Init(_LPC_ADC_ID, &ADCSetup);
	Chip_ADC_EnableChannel(_LPC_ADC_ID, _ADC_CHANNLE, ENABLE);
	// Habilita interrupcion dato entrada UART
	Chip_UART_IntEnable(UART_SELECTION, (UART_IER_RBRINT ));
	NVIC_SetPriority(IRQ_SELECTION, 1);
	NVIC_EnableIRQ(IRQ_SELECTION);
	DEBUGOUT("Hardware OK \n");			//Mensaje inicial.
}

//Tarea productor 1: UART
/*
 * El
*primero   de   los   productores   es   una   tarea
*que   genera   strings   de   caracteres   de   longitud
*variable   (ingreso   de   comandos   por   teclado).
 * */
static void vProductor1Task(void *pvParameters) {
	while(1){
	Chip_UART_IntDisable(UART_SELECTION, (UART_IER_RBRINT ));
	int intentando = 1; //Llave que me va a decir su pudo tomar o no el semaforo. Si no lo tomo, sigue tratando hasta que pueda
	while (intentando) {
			if( xSemaphore != NULL ){
			/* Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY */
				if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
				{/* Obtenido el semaforo, manejamos el recurso compartido*/
					if(index>=5){
						DEBUGOUT("Desborde de pila task1UART \r\n");
					}
					else{
		            	struct elemeto elem;
		            	palabraElejir +=1;
		            	if(palabraElejir>5){
		            		palabraElejir=0;
		            	}
		            	memset(elem.palabra, '\0', 9);
		            	strcpy(elem.palabra, arregloPalabras[palabraElejir]);
						pila[index]=elem;
						DEBUGOUT("ProductorUART: %d || %s\r\n", index, pila[index].palabra);
						index=index+1;
						}
					/* Finalizado el uso del recurso compartido, devolvemos el semaforo */
					xSemaphoreGive( xSemaphore );
				}
				else
				{
				/* Se ingresa aca si no se pudo obtener el semaforo*/
				}
			}
	intentando = 0;
	}//fin while(intentando)
	Chip_UART_IntEnable(UART_SELECTION, (UART_IER_RBRINT ));
	vTaskSuspend(NULL);	//Suspende su tarea
	}//Fin while(1)
}

//Tarea productor 2: ADC
static void vProductor2Task(void *pvParameters) {
	while (1) {
		uint16_t dataADC;
		/* Select using burst mode or not ¿hace falta? */
		if (Burst_Mode_Flag) {
			Chip_ADC_SetBurstCmd(_LPC_ADC_ID, ENABLE);
		}
		else {
			Chip_ADC_SetBurstCmd(_LPC_ADC_ID, DISABLE);
		}
			/* Start A/D conversion if not using burst mode */
		if (!Burst_Mode_Flag) {
			Chip_ADC_SetStartMode(_LPC_ADC_ID, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
		}
			/* Waiting for A/D conversion complete */
		while (Chip_ADC_ReadStatus(_LPC_ADC_ID, _ADC_CHANNLE, ADC_DR_DONE_STAT) != SET) {
		}
		/* Read ADC value */
		Chip_ADC_ReadValue(_LPC_ADC_ID, _ADC_CHANNLE, &dataADC);
		/* Disable burst mode, if any */
		if (Burst_Mode_Flag) {
			Chip_ADC_SetBurstCmd(_LPC_ADC_ID, DISABLE);
		}
		//Ya tengo el valor del ADC, lo paso a string para almacenarlo en la lista
		//sprintf(valorAdc, "%u", dataADC);//Revisar. Aca se rompe
		//Teniendo el resultado, pido el semaforo
		if( xSemaphore != NULL )
						    {
						        /* Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY */
						        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
						        {
						            /* Obtenido el semaforo, manejamos el recurso compartido*/

						        	if(index>=5){
						        		DEBUGOUT("Desborde de pila task2 ADC \r\n");
						        		//Se pierde ultimo valor \n");(no sobreescribe)
						        	}
						        	else{
						        		//DEBUGOUT("Copiando a la pila: %s \n", cadenaUart);
						            	struct elemeto elem;
						            	elem.letra=dataADC;
						            	memset(elem.palabra, '\0', 9);
										//pila[index]=(char*)malloc(strlen(valorAdc));
										//strcpy(pila[index], valorAdc);
							            pila[index]=elem;
						            	DEBUGOUT("ProductorADC: %d || %d \r\n",index, pila[index].letra);
						        		index=index+1;
						        	} /* Finalizado el uso del recurso compartido, devolvemos el semaforo */
						            xSemaphoreGive( xSemaphore );
						        }
						        else
						        {
						            /* Se ingresa aca si no se pudo obtener el semaforo*/
						        }
		// About a 3Hz on/off toggle rate (no mover de aca)
		vTaskDelay(DEMORA_PROD_ADC);
		}
}//fin while(1)
}

//Tarea consumidor
/*el   consumidor,   es   una   tarea   que   envía   el   valor
 *recibido   a   la   terminal   de   una
 *computadora   por   puerto   serie   (UART)
 * */
static void vConsumidorTask(void *pvParameters) {
	while (1) {
				if( xSemaphore != NULL )
				    {   // Verifica si el semaforo esta libre. si no, espera portMAX_DELAY.
				        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
				        {   // Otenido el semaforo, manipulamos el recurso compartido
							contador=contador+1;
							if(contador==LIMITE_TRACE){
					            Board_LED_Set(0, true);		//Prende LED
								vTraceStop();
							}
				            if(index>0){
				            	index=index-1;
				            }
				            if(index<0){
				            	DEBUGOUT("\n **EPA, NO DEBERIAS VER ESTO CONSUMIDOR** \n");
				            	DEBUGOUT("Indice: %d", index);
				            }
				            DEBUGOUT("Consumidor: %d||%s||%d\r\n",index, pila[index].palabra,pila[index].letra );
				            // Liberado el recurso, devolvemos el semaforo
				            xSemaphoreGive( xSemaphore );
				        }
				        else
				        {
				            // Se ingresa aqui si no se pudo obtener el semaforo
				        }
				    }
		// About a 7Hz on/off toggle rate
		vTaskDelay(DEMORA_CONS);
	}
}

void HANDLER_NAME(void)
{
	Chip_UART_IntDisable(UART_SELECTION, (UART_IER_RBRINT ));
	int caracter = Board_UARTGetChar();
	vTaskResume(taskProductor1_handle);//Pone el task productor 1 en la cola
	Chip_UART_IntEnable(UART_SELECTION, (UART_IER_RBRINT ));
}

int main(void)
{
	vTraceInitTraceData();
	uiTraceStart();
	prvSetupHardware();
	xSemaphore = xSemaphoreCreateMutex(); //Inicializa mutex
	/* Crea tarea productor 1 */

	xTaskCreate(vProductor1Task, (signed char *) "vProductor1Task",
				configMINIMAL_STACK_SIZE, NULL,1,
				&taskProductor1_handle);
				//(xTaskHandle *) NULL);

	/* Crea tarea productor 2 */
	xTaskCreate(vProductor2Task, (signed char *) "vProductor2Task",
				configMINIMAL_STACK_SIZE, NULL,1,
				(xTaskHandle *) NULL);

	/* Crea tarea consumidor*/
	xTaskCreate(vConsumidorTask, (signed char *) "vConsumidorTask",
				configMINIMAL_STACK_SIZE, NULL, 1,
				(xTaskHandle *) NULL);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}
