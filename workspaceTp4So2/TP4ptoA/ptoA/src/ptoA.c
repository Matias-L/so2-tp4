#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"

xSemaphoreHandle xSemaphore = NULL;//Semaforo como variable global
size_t acumulador=0;

/* Sets up system hardware */
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);
}

//Tarea productor
static void vProductorTask(void *pvParameters) {
	while (1) {

		if( xSemaphore != NULL )
		    {
		        /* Verifica si el semaforo esta disponible. Si no, espera portMAX_DELAY */
		        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
		        {
		            /* Obtenido el semaforo, manejamos el recurso compartido*/

		            acumulador=acumulador+1;
		            Board_LED_Set(0, true);
		            DEBUGOUT("Productor: %d\r\n", acumulador);
		            //printf("%d", acumulador);
		            //LedState = (bool) !LedState;

		            /* Finalizado el uso del recurso compartido, devolvemos el semaforo */
		            xSemaphoreGive( xSemaphore );
		        }
		        else
		        {
		            /* Se ingresa aca si no se pudo obtener el semaforo*/
		        }
		    }

		/* About a 3Hz on/off toggle rate */
		vTaskDelay(configTICK_RATE_HZ / 6);
	}
}

//Tarea consumidor
static void vConsumidorTask(void *pvParameters) {
	while (1) {

				if( xSemaphore != NULL )
				    {
				        /* Verifica si el semaforo esta libre. si no, espera portMAX_DELAY. */
				        if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )
				        {
				            /* Otenido el semaforo, manipulamos el recurso compartido*/

				            if(acumulador>0){
							acumulador=acumulador-1;
							//printf("%d", acumulador);
				            Board_LED_Set(1, false);
				            DEBUGOUT("Consumidor: %d\r\n", acumulador);
				            }

				            /* Liberado el recurso, devolvemos el semaforo*/
				            xSemaphoreGive( xSemaphore );
				        }
				        else
				        {
				            /* Se ingresa aqui si no se pudo obtener el semaforo */
				        }
				    }

		/* About a 7Hz on/off toggle rate */
		vTaskDelay(configTICK_RATE_HZ / 14);
	}
}

/* UART (or output) thread */
/*
static void vUARTTask(void *pvParameters) {
	int tickCnt = 0;

	while (1) {
		DEBUGOUT("Tick: %d\r\n", tickCnt);
		tickCnt++;

		// About a 1s delay here
		vTaskDelay(configTICK_RATE_HZ);
	}
}
*/
/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	main routine for FreeRTOS blinky example
 * @return	Nothing, function should not exit
 */
int main(void)
{
	prvSetupHardware();
	xSemaphore = xSemaphoreCreateMutex(); //Inicializa mutex

	/* Crea tarea productor */
	xTaskCreate(vProductorTask, (signed char *) "vProductorTask",
				configMINIMAL_STACK_SIZE, NULL,1,
				(xTaskHandle *) NULL);

	/* Crea tarea consumidor*/
	xTaskCreate(vConsumidorTask, (signed char *) "vConsumidorTask",
				configMINIMAL_STACK_SIZE, NULL, 1,
				(xTaskHandle *) NULL);

	 //UART output thread, simply counts seconds
	/*xTaskCreate(vUARTTask, (signed char *) "vTaskUart",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);
*/
	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

/**
 * @}
 */
