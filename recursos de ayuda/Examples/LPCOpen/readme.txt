LPCOpen Software Bundles
========================
LPCOpen is an extensive collection of software libraries for LPC MCUs,
and incorporates peripheral drivers, middleware, and examples.

With LPCOpen you can quickly and easily utilize an extensive array of
software drivers and libraries in order to create and develop
multifunctional products.

This directory contains several LPCOpen software bundles for use with
the MCUXpresso IDE and a variety of development boards.

Note that LPCOpen bundles are periodically updated, and additional
bundles are released. Thus we would always recommend checking the
LPCOpen pages to ensure that you are using the latest versions.

For the latest LPCOpen downloads and for more information on LPCOpen,
visit:
http://www.nxp.com/lpcopen
