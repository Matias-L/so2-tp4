Here in the LPC team we are really excited re-energizing the LPC800 family and are
working hard to help customers who want to make the transition from 8- and 16-bit MCUs
to the Cortex M0/M0+.

To this end we've created code bundles, which consist of software examples to teach users
how to program the peripherals at the basic level. The examples provide register-level
peripheral access, and direct correspondence to the memory map in the User Manual.
Examples are concise, and accurate explanations are provided within the readmes and
comments in source files.

This directory contains initial versions of these bundles, and you can find the
latest versions at https://www.nxp.com/LPC800-Code-Bundles

