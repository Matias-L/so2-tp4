Legacy Software Bundles
=======================
This directory contains various legacy CMSIS / Peripheral Driver
Library / code bundle software packages for use with the LPCXpresso
IDE and a variety of development boards.

Generally these legacy packages should only be used for existing 
development work. When starting a new evaluation or product
development, LPCOpen is now the preferred software platform for most
NXP Cortex-M based MCUs.

You can find some LPCOpen software bundles in:
<install_dir>\lpcxpresso\Examples\LPCOpen

but for the latest LPCOpen downloads and for more information on
LPCOpen, visit:

http://www.lpcware.com/lpcopen
